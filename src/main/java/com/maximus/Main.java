package com.maximus;

import com.google.common.base.Stopwatch;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main {

    public static final String DEFAULT_FILE_NAME = "intervals.json";

    public static void main(String[] args) throws IOException {
        showWelcomeMessage();
        String fileName = getFileNameFromUserInput();

        Stopwatch stopwatch = Stopwatch.createStarted();

        List<Interval> intervals = IntervalProcessor.getIntervalsFromJSON(fileName);
        printIntervalsDetails(intervals, "Available intervals: " + intervals.size());
        IntervalProcessor.validateIntervals(intervals);
        List<Interval> separateIntervals = IntervalProcessor.getSeparateIntervals(intervals);
        printIntervalsDetails(separateIntervals, "Separate intervals: " + separateIntervals.size());

        System.out.println("Time: " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms");
    }

    private static String getFileNameFromUserInput() {
        String input = new Scanner(System.in).nextLine();
        if (!input.trim().equals("1")) {
            return input;
        } else {
            return DEFAULT_FILE_NAME;
        }
    }

    private static void showWelcomeMessage() {
        System.out.println("Please enter JSON file name or path from which you want to get intervals and press 'Enter'.\n" +
                "For default file name (intervals.json) type '1' and press 'Enter'");
    }

    private static void printIntervalsDetails(List<Interval> intervals, String message) {
        System.out.println("-------- " + message + " --------");
        intervals.forEach(System.out::println);
        System.out.println("---------------------------------------");
    }

}
