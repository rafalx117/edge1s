package com.maximus;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;

public class IntervalProcessor {

    public static List<Interval> getSeparateIntervals(List<Interval> intervals) {

        List<Interval> separateIntervals = new ArrayList<>();
        intervals.sort(new IntervalComparator());
        Interval expandableInterval = intervals.get(0);
        Interval lastInterval = intervals.get(intervals.size() - 1);

        for (Interval currentInterval : intervals) {
            boolean lastIteration = currentInterval == lastInterval;

            if (expandableInterval.canBeExtendedBy(currentInterval)) {
                expandableInterval.setEnd(currentInterval.getEnd());
            } else if (!currentInterval.isInside(expandableInterval)) {
                separateIntervals.add(expandableInterval);
                expandableInterval = currentInterval;
            }

            if (lastIteration) {
                separateIntervals.add(expandableInterval);
            }
        }
        return separateIntervals;
    }

    public static void validateIntervals(List<Interval> intervals) throws InvalidObjectException {
        if (intervals.isEmpty()) {
            throw new InvalidObjectException("Interval set is empty!");
        }

        for (Interval interval : intervals) {
            if (interval.getEnd() < interval.getStart()) {
                throw new InvalidObjectException("Interval: " + interval + " is not valid!");
            }
        }
    }

    public static List<Interval> getIntervalsFromJSON(String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new FileReader(fileName), new TypeReference<List<Interval>>() {
        });
    }

}
