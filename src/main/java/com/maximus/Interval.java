package com.maximus;

public class Interval {
    private int start;
    private int end;

    public Interval() {
    }

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public boolean isInside(Interval interval) {
        return this.getStart() >= interval.getStart()
                && this.getEnd() <= interval.getEnd();
    }

    public boolean hasCommonPartWith(Interval interval) {
        if (this.getStart() <= interval.getStart()) {
            return interval.getStart() <= this.getEnd();
        } else {
            return this.getStart() <= interval.getEnd();
        }
    }

    public boolean canBeExtendedBy(Interval interval) {
        return this.hasCommonPartWith(interval) && interval.getEnd() > this.getEnd();
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "<" + getStart() + "," + getEnd() + ">";
    }

}
