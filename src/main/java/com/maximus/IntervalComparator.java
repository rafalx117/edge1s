package com.maximus;

import java.util.Comparator;

public class IntervalComparator implements Comparator<Interval> {
    @Override
    public int compare(Interval interval, Interval anotherInterval) {
        return interval.getStart() - anotherInterval.getStart();
    }

}
