import com.maximus.Interval;
import com.maximus.IntervalProcessor;
import com.maximus.Main;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.ArrayList;
import java.util.List;

public class MainTest {

    private IntervalRepository repository = null;

    @Before
    public void init() {
        repository = new SimpleIntervalRepository();
    }

    @Test
    public void JSONShouldBeParsed() {
        List<Interval> intervals = null;
        try {
            intervals = IntervalProcessor.getIntervalsFromJSON(Main.DEFAULT_FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.assertNotNull(intervals);
    }

    @Test(expected = FileNotFoundException.class)
    public void intervalProcessorShouldThrowFOFException() throws IOException {
        IntervalProcessor.getIntervalsFromJSON("xD");
    }

    @Test
    public void validationShouldNotCauseException() throws InvalidObjectException {
        List<Interval> intervals = repository.getSingleElementIntervalsList();
        IntervalProcessor.validateIntervals(intervals);
    }

    @Test(expected = InvalidObjectException.class)
    public void validationShouldCauseInvalidObjectException() throws InvalidObjectException {
        IntervalProcessor.validateIntervals(new ArrayList<>());
    }

    @Test(expected = InvalidObjectException.class)
    public void validationShouldCauseInvalidObjectException2() throws InvalidObjectException {
        List<Interval> intervals = repository.getInvalidSingleElementIntervalList();
        IntervalProcessor.validateIntervals(intervals);
    }

    @Test
    public void intervalProcessorShouldReturnIntervalsList() {
        List<Interval> intervals = repository.getUnmergeableIntervals();
        List<Interval> output = IntervalProcessor.getSeparateIntervals(intervals);

        Assert.assertArrayEquals(new List[]{intervals}, new List[]{output});
    }

    @Test
    public void intervalProcessorShouldReturnOneInterval() {
        List<Interval> intervals = repository.getSingleElementIntervalsList();
        List<Interval> output = IntervalProcessor.getSeparateIntervals(intervals);

        Assert.assertArrayEquals(new List[]{intervals}, new List[]{output});
    }

    @Test
    public void intervalProcessorShouldReturnMergedInterval() {
        List<Interval> intervals = repository.getMergeableIntervals();
        Interval expectedResult = ((SimpleIntervalRepository) repository).getExpectedInterval();
        List<Interval> output = IntervalProcessor.getSeparateIntervals(intervals);

        Assert.assertEquals(expectedResult.toString(), output.get(0).toString()); //Konieczne było dodatnie toString, ponieważ asercja z porównaniem obiektów nie przechodziła
    }

    @Test
    public void intervalsShouldHaveCommonPart() {
        Interval interval = new Interval(1, 5);
        Interval anotherInterval = new Interval(5, 7);

        Assert.assertTrue(interval.hasCommonPartWith(anotherInterval));
        Assert.assertTrue(anotherInterval.hasCommonPartWith(interval));
    }

    @Test
    public void intervalsShouldHaveCommonPart_2() {
        Interval interval = new Interval(1, 5);
        Interval anotherInterval = new Interval(3, 7);

        Assert.assertTrue(interval.hasCommonPartWith(anotherInterval));
        Assert.assertTrue(anotherInterval.hasCommonPartWith(interval));
    }

    @Test
    public void intervalsShouldNotHaveCommonPart() {
        Interval interval = new Interval(1, 5);
        Interval anotherInterval = new Interval(6, 7);

        Assert.assertFalse(interval.hasCommonPartWith(anotherInterval));
        Assert.assertFalse(anotherInterval.hasCommonPartWith(interval));
    }

    @Test
    public void intervalShouldBeExtended() {
        Interval intervalToExtend = repository.getIntervalToExtend();
        Interval extendingInterval = repository.getExtendingInterval();

        Assert.assertTrue(intervalToExtend.canBeExtendedBy(extendingInterval));
    }

    @Test
    public void intervalShouldNotBeExtended() {
        Interval intervalToExtend = repository.getIntervalToExtend();
        Interval notExtendingInterval = repository.getNotExtendingInterval();

        Assert.assertFalse(intervalToExtend.canBeExtendedBy(notExtendingInterval));
    }

    @Test
    public void intervalShouldBeInside() {
        Interval inner = repository.getInnerInterval();
        Interval outer = repository.getOuterInterval();

        Assert.assertTrue(inner.isInside(outer));
    }

    @Test
    public void intervalShouldNotBeInside() {
        Interval inner = repository.getInnerInterval();
        Interval outer = repository.getOuterInterval();

        Assert.assertFalse(outer.isInside(inner));
    }

}
