import com.maximus.Interval;

import java.util.ArrayList;
import java.util.List;

public class SimpleIntervalRepository implements IntervalRepository {
    private Interval expectedInterval;

    @Override
    public List<Interval> getMergeableIntervals() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 4));
        intervals.add(new Interval(3, 7));
        setExpectedInterval(new Interval(1, 7));
        return intervals;
    }

    @Override
    public List<Interval> getUnmergeableIntervals() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 4));
        intervals.add(new Interval(5, 7));
        return intervals;
    }

    @Override
    public List<Interval> getSingleElementIntervalsList() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(1, 2));
        return intervals;
    }

    @Override
    public List<Interval> getInvalidSingleElementIntervalList() {
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(5, 0));
        return intervals;
    }

    @Override
    public Interval getIntervalToExtend() {
        return new Interval(1, 10);
    }

    @Override
    public Interval getExtendingInterval() {
        return new Interval(5, 15);
    }

    @Override
    public Interval getNotExtendingInterval() {
        return new Interval(2, 5);
    }

    @Override
    public Interval getOuterInterval() {
        return new Interval(1, 10);
    }

    @Override
    public Interval getInnerInterval() {
        return new Interval(2, 6);
    }

    private void setExpectedInterval(Interval expectedInterval) {
        this.expectedInterval = expectedInterval;
    }

    public Interval getExpectedInterval() {
        return expectedInterval;
    }

}
