import com.maximus.Interval;

import java.util.List;

public interface IntervalRepository {

    List<Interval> getMergeableIntervals();

    List<Interval> getUnmergeableIntervals();

    List<Interval> getSingleElementIntervalsList();

    List<Interval> getInvalidSingleElementIntervalList();

    Interval getIntervalToExtend();

    Interval getExtendingInterval();

    Interval getNotExtendingInterval();

    Interval getOuterInterval();

    Interval getInnerInterval();

}
